# Use an official Nginx runtime as a parent image
FROM nginx:latest

# Set environment variable for the port
ARG PORT=8080
ENV PORT=$PORT

# Expose the specified port
EXPOSE $PORT

# Copy custom configuration files, if needed
# COPY nginx.conf /etc/nginx/nginx.conf

# CMD to start Nginx with the specified port
CMD ["nginx", "-g", "daemon off;"]